﻿namespace App.Core
{
    /// <summary>
    /// Provide public application common information.
    /// </summary>
    public static class Common
    {
        /// <summary>
        /// The application business libraries prefix
        /// </summary>
        public const string ApplicationBusinessLibrariesPrefix = "App.Lib";

        /// <summary>
        /// The application relative path for login.
        /// </summary>
        public const string ApplicationRelativePathLogin = "/Account/Login";

        /// <summary>
        /// The application relative path for logout.
        /// </summary>
        public const string ApplicationRelativePathLogout = "/Account/Logout";
    }
}